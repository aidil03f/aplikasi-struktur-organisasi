<?php

use App\Http\Controllers\{CompanyController, EmployeeController, PositionController};
use Illuminate\Support\Facades\Route;

Route::view('/','dashboard');

Route::resources([
    'companies' => CompanyController::class,
    'positions' => PositionController::class,
    'employees' => EmployeeController::class
]);
Route::get('exportPDF',[EmployeeController::class,'exportPDF']);
Route::get('exportExcel',[EmployeeController::class,'exportExcel']);
Route::get('exportPDFcompany',[CompanyController::class,'exportPDF']);
Route::get('exportExcelcompany',[CompanyController::class,'exportExcel']);
@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card col-5 p-0">
        <div class="card-header">New Company</div>
        <div class="card-body">
            <form action="{{ route('companies.store') }}" method="post" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                    @error('name')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                   <label for="address">Address</label>
                   <textarea name="address" id="address" rows="3" class="form-control"></textarea>
                    @error('address')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary ml-1" style="float:right"><i class="fas fa-save"></i> Save</button>
                    <a href="{{ route('companies.index') }}" class="btn btn-danger" style="float:right">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>alamat</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $company)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $company->nama }}</td>
                <td>{{ $company->alamat }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
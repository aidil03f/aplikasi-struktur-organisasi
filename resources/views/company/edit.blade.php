@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card col-5 p-0">
        <div class="card-header">Edit Company</div>
        <div class="card-body">
            <form action="{{ route('companies.update',$company) }}" method="post" autocomplete="off">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" value="{{ $company->nama }}" class="form-control">
                    @error('name')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                   <label for="address">Address</label>
                   <textarea name="address" id="address" rows="3" class="form-control">{{ $company->alamat }}</textarea>
                    @error('address')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-warning ml-1" style="float:right">Update</button>
                    <a href="{{ route('companies.index') }}" class="btn btn-danger" style="float:right">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
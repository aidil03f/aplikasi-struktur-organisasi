@extends('layouts.app',['title'=> 'Companies'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('companies.create') }}" class="btn btn-primary btn-sm ml-5" style="float:right">Tambah</a>
        <a href="{{ url('exportExcelcompany') }}" class="btn btn-outline-success btn-sm ml-2" style="float:right">Excel</a>
        <a href="{{ url('exportPDFcompany') }}" class="btn btn-outline-danger btn-sm ml-1" style="float:right">PDF</a>
        <h5>Company</h5>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr> 
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $company)    
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $company->nama }}</td>
                        <td>{{ $company->alamat }}</td>
                        <td>
                            <form action="{{ route('companies.destroy',$company) }}" method="post">
                                <a href="{{ route('companies.edit',$company) }}" class="btn btn-warning btn-sm" title="edit ?"><i class="fas fa-edit"></i></a>
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure ?')" title="delete ?"><i class="fas fa-trash"></i></button>
                           </form>
                        </td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


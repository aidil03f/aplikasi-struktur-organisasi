@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card col-5 p-0">
        <div class="card-header">Edit Position</div>
        <div class="card-body">
            <form action="{{ route('positions.update',$position) }}" method="post" autocomplete="off">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" value="{{ $position->nama }}" class="form-control">
                    @error('name')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-warning ml-1" style="float:right">Update</button>
                    <a href="{{ route('positions.index') }}" class="btn btn-danger" style="float:right">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
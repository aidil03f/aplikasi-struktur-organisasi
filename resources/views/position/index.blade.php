@extends('layouts.app',['title'=> 'Positions'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('positions.create') }}" class="btn btn-primary btn-sm ml-5" style="float:right">Tambah</a>
        <h5>Position</h5>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr> 
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($positions as $position)    
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $position->nama }}</td>
                        <td>
                            <form action="{{ route('positions.destroy',$position) }}" method="post">
                                <a href="{{ route('positions.edit',$position) }}" class="btn btn-warning btn-sm" title="edit ?"><i class="fas fa-edit"></i></a>
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure ?')" title="delete ?"><i class="fas fa-trash"></i></button>
                           </form>
                        </td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- @include('kategori.form') --}}
@endsection


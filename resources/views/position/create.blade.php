@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card col-5 p-0">
        <div class="card-header">New Position</div>
        <div class="card-body">
            <form action="{{ route('positions.store') }}" method="post" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                    @error('name')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary ml-1" style="float:right"><i class="fas fa-save"></i> Save</button>
                    <a href="{{ route('positions.index') }}" class="btn btn-danger" style="float:right">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
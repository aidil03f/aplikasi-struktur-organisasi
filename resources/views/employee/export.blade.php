<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Posisi</th>
            <th>Perusahaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $employee)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $employee->nama }}</td>
                <td>{{ $employee->position->nama }}</td>
                <td>{{ $employee->company->nama }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
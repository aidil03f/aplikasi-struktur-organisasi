@extends('layouts.app',['title'=> 'Employees'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('employees.create') }}" class="btn btn-primary btn-sm ml-5" style="float:right">Tambah</a>
        <a href="{{ url('exportExcel') }}" class="btn btn-outline-success btn-sm ml-2" style="float:right">Excel</a>
        <a href="{{ url('exportPDF') }}" class="btn btn-outline-danger btn-sm ml-1" style="float:right">PDF</a>
        <h5>Employee</h5>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr> 
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Posisi</th>
                        <th>Perusahaan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $employee)    
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $employee->nama }}</td>
                        <td>{{ $employee->position->nama }}</td>
                        <td>{{ $employee->company->nama }}</td>
                        <td>
                            <form action="{{ route('employees.destroy',$employee) }}" method="post">
                                <a href="{{ route('employees.edit',$employee) }}" class="btn btn-warning btn-sm" title="edit ?"><i class="fas fa-edit"></i></a>
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure ?')" title="delete ?"><i class="fas fa-trash"></i></button>
                           </form>
                        </td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


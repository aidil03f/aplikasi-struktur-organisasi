@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card col-5 p-0">
        <div class="card-header">New Employee</div>
        <div class="card-body">
            <form action="{{ route('employees.store') }}" method="post" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                    @error('name')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control">
                        <option disabled selected>Choose One!</option>
                        @foreach ($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->nama }}</option>
                        @endforeach
                    </select>
                    @error('company')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                 <div class="form-group">
                    <label for="position">Position</label>
                    <select name="position" id="position" class="form-control">
                        <option disabled selected>Choose One!</option>
                        @foreach ($positions as $position)
                            <option value="{{ $position->id }}">{{ $position->nama }}</option>
                        @endforeach
                    </select>
                    @error('position')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary ml-1" style="float:right"><i class="fas fa-save"></i> Save</button>
                    <a href="{{ route('employees.index') }}" class="btn btn-danger" style="float:right">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
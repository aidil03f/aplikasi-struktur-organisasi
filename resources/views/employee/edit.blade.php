@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card col-5 p-0">
        <div class="card-header">Edit Employee</div>
        <div class="card-body">
            <form action="{{ route('employees.update',$employee) }}" method="post" autocomplete="off">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" value="{{ $employee->nama }}" class="form-control">
                    @error('name')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control">
                        @foreach ($companies as $company)
                            <option {{ $company->id == $employee->company_id ? 'selected' : '' }} value="{{ $company->id }}">{{ $company->nama }}</option>
                        @endforeach
                    </select>
                    @error('company')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                 <div class="form-group">
                    <label for="position">Position</label>
                    <select name="position" id="position" class="form-control">
                        @foreach ($positions as $position)
                            <option {{ $position->id == $employee->position_id ? 'selected' : '' }} value="{{ $position->id }}">{{ $position->nama }}</option>
                        @endforeach
                    </select>
                    @error('position')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-warning ml-1" style="float:right">Update</button>
                    <a href="{{ route('employees.index') }}" class="btn btn-danger" style="float:right">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
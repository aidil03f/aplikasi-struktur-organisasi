<?php

namespace App\Exports;
use App\Models\Employee;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EmployeeExport implements FromView
{
    public function view() : View
    {
         return view('employee.export', [
                'data' => Employee::all()
            ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function index()
    {
        $positions = Position::get();
        return view('position.index',compact('positions'));
    }

    public function create()
    {
        return view('position.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        Position::create([
            'nama' => $request->name
        ]);
        session()->flash('success','The position has been created successfully');
        return redirect()->route('positions.index');
    }

    public function edit($id)
    {
        return view('position.edit',[
            'position' => Position::find($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $position = Position::findOrFail($id);
        $position->update([
            'nama' => $request->name
        ]);
        session()->flash('success','The position has been updated successfully');
        return redirect()->route('positions.index');
    }
    
    public function destroy($id)
    {
        Position::find($id)->delete();
        return redirect()->route('positions.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\{Company,Employee,Position};
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class EmployeeController extends Controller
{
  
    public function index()
    {
        $employees = Employee::get();
        return view('employee.index',compact('employees'));
    }

    public function create()
    {
        return view('employee.create',[
            'positions' => Position::get(),
            'companies' => Company::get()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'company' => 'required',
            'position' => 'required'
        ]);
        Employee::create([
            'nama' => $request->name,
            'company_id' => $request->company,
            'position_id' => $request->position
        ]);
        session()->flash('success','The employee has been created successfully');
        return redirect()->route('employees.index');
    }

    public function edit($id)
    {
        return view('employee.edit',[
            'employee' => Employee::find($id),
            'companies' => Company::get(),
            'positions' => Position::get()
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'company' => 'required',
            'position' => 'required'
        ]);
        $employee = Employee::findOrFail($id);
        $employee->update([
            'nama' => $request->name,
            'company_id' => $request->company,
            'position_id' => $request->position
        ]);
        session()->flash('success','The employee has been updated successfully');
        return redirect()->route('employees.index');
    }

    public function destroy($id)
    {
        Employee::find($id)->delete();
        return redirect()->route('employees.index');
    }

    public function exportExcel()
    {
        return Excel::download(new EmployeeExport, 'Employee.xlsx');
    }
    public function exportPDF()
    {       
        return Excel::download(new EmployeeExport, 'Employee.pdf');
    }
}

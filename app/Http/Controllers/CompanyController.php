<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompanyExport;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::get();
        return view('company.index',compact('companies'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required'
        ]);
        Company::create([
            'nama' => $request->name,
            'alamat' => $request->address
        ]);
        session()->flash('success','The company has been created successfully');
        return redirect()->route('companies.index');
    }

    public function edit($id)
    {
        return view('company.edit',[
            'company' => Company::find($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required'
        ]);
        $company = Company::findOrfail($id);
        $company->update([
            'nama' => $request->name,
            'alamat' => $request->address
        ]);
        session()->flash('success','The company has been updated successfully');
        return redirect()->route('companies.index');
    }

    public function exportExcel()
    {
        return Excel::download(new CompanyExport, 'Company.xlsx');
    }
    public function exportPDF()
    {       
        return Excel::download(new CompanyExport, 'Company.pdf');
    }

    public function destroy($id)
    {
        Company::find($id)->delete();
        return redirect()->route('companies.index');
    }
}

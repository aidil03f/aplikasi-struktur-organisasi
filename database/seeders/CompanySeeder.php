<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
   
    public function run()
    {
        collect([
            [
                'nama' => 'PT JAVAN',
                'alamat' => 'Sleman'
            ],
            [
                'nama' => 'PT Dicoding',
                'alamat' => 'Bandung'
            ]
        ])->each(function($company){
            Company::create($company);
        });

    }
}

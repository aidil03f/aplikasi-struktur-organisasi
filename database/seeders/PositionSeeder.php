<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            'CEO',
            'Direktur',
            'Manager',
            'Staff'
        ])->each(function($data){
            Position::create([
                'nama' => $data
            ]);
        });
    }
}

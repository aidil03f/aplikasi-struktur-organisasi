<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'nama' => 'Pak Budi',
                'position_id' => '1',
                'company_id' => '1'
            ],
            [
                'nama' => 'Pak Tono',
                'position_id' => '2',
                'company_id' => '1'
            ],
            [
                'nama' => 'Pak Totok',
                'position_id' => '2',
                'company_id' => '1'
            ],
            [
                'nama' => 'Bu Sinta',
                'position_id' => '3',
                'company_id' => '1'
            ],
            [
                'nama' => 'Bu Novi',
                'position_id' => '3',
                'company_id' => '1'
            ],
            [
                'nama' => 'Andre',
                'position_id' => '4',
                'company_id' => '1'
            ],
            [
                'nama' => 'Doni',
                'position_id' => '4',
                'company_id' => '1'
            ],
            [
                'nama' => 'Ismir',
                'position_id' => '4',
                'company_id' => '1'
            ],
            [
                'nama' => 'Anto',
                'position_id' => '4',
                'company_id' => '1'
            ]
        ])->each(function($data){
            Employee::create($data);
        });
    }
}
